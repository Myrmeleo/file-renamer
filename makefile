NAME = "rename"

default:
	@echo "Compiling..."
	javac -sourcepath src *.java
	javac -sourcepath src *.java -d out/src
	jar cmf manifest.mf out/$(NAME).jar -C out/src .

run: default
	@echo "Running..."
	java -jar out/$(NAME).jar

clean:
	rm -rf *.class