# File Renamer

This program renames files and is intended to be run from the command terminal

Made with openjdk version "12.0.2" 2019-07-16
On Microsoft Windows 10 Pro 10.0.17763 N/A Build 17763

-----SETUP-----

Run the makefile and you'll be able to run the program with either the .java file, or the .jar file produced in the new /out folder.  

In the command line, type java rename [options] if using the .java file, or java -jar rename.jar [options] if using the .jar file.  

THE FILE YOU WANT TO RENAME MUST BE IN THE SAME DIRECTORY AS THE .JAVA OR .JAR FILE YOU ARE RUNNING  
 

-----INSTRUCTIONS-----  

Options are as follows:  
    -help                     :: print out a help page and exit the program.  
    -prefix [string]          :: rename [filename] so that it starts with [string].   
    -suffix [string]          :: rename [filename] so that it ends with [string].   
    -replace [str1] [str2]    :: rename [filename] by replacing all instances of [str1] with [str2].   
    -file [filename]          :: denotes the [filename] to be modified.  
    
Arguments are parameters for each option, and always follow options on the command-line. All options except `-help' require arguments to be operate.  
Arguments cannot start with a dash ("-"), since that symbol is used to designate options. Arguments consist of one of the following:  
    - a string, or  
    - @date which should be replaced by your program with the current date as a string (MM-DD-YYYY format), or  
    - @time which should be replaced by your program with the current time as a string (HH-MM-SS format), or  
    - any combination of these separated by a space (ie. multiple arguments to a single option). Note that only some options support multiple arguments.  

Specifying multiple arguments to -prefix' or -suffix' denotes that the arguments should be concatenated together, and passed to the option as a single argument (e.g. -prefix a b c' is the same as '-prefix abc').  
The '-replace' option must be passed exactly two arguments.  

The 'file' option can support multiple arguments, representing multiple filenames. If more than a single filename is passed, it should be assumed that the operations are performed on all files. A valid filename argument can consist of:  
    - a string representing a single file, or  
    - multiple strings representing multiple files, separated by spaces,  
    - multiple files specified using using an asterisk (*) or question mark (?).  

Every call requires at least one '-file' option along with at least one other option.  
You can perform multiple operations at once e.g. '-prefix' and '-replace' can both be applied to one or more files with a single call. The exception to this is '-help’, which will cause other arguments to be ignored, and help to be displayed instead.  
Options can be specified in any order. i.e. 'java rename -prefix a -file f1' and 'java rename -file f1 -prefix a' are equally valid and represent the same operation.   