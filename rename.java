// Leo Huang
// File Renamer cmd line program

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.io.File;

class rename {

    static class Option {
        OptionType which;
        String arg;
        Option(OptionType o, String a) {
            which = o;
            arg = a;
        }
        OptionType getType () {return which;}
        String getArg () {return arg;}
    }

    //HELP is not listed as it does not need to be recorded in operations
    //FILE is not listed as it has its own arraylist
    enum OptionType {PREFIX, SUFFIX, REPLACE}
    //options will be applied in the order listed on operations
    //The value can either be a string for prefix or suffix, or an index (int.tostring) for the element to replace
    static ArrayList<Option> operations = new ArrayList<>(0);
    static ArrayList<String> toBeReplaced = new ArrayList<>(0);
    static ArrayList<String> toReplaceWith = new ArrayList<>(0);
    //give warning if string is not found
    static ArrayList<Boolean> replacedStrFound = new ArrayList<>(0);
    static ArrayList<File> files = new ArrayList<>(0);

    static String helpMsg = "\nLeo Huang 2019. Last revised: 12-09-19\n" +
            "Usage: java rename [-option argument1 argument2 ...]\n\n" +
            "Options:\n" +
            "-help\t\t\t\t:: display this help\n" +
            "-prefix [string]\t\t:: rename [filename] so that it starts with [string].\n" +
            "  -suffix [string]\t\t:: rename [filename] so that it ends with [string].\n" +
            "  -replace [str1] [str2]\t:: rename [filename] by replacing all instances of [str1] with [str2].\n" +
            "  -file [filename]\t\t:: denotes the [filename] to be modified.";

    static SimpleDateFormat datef = new SimpleDateFormat("MM-dd-yyyy");
    static SimpleDateFormat timef = new SimpleDateFormat("HH-mm-ss");

    //formatDT searches for @date and @time in in and replaces with formatted date and time
    public static String formatDT(String in) {
        Date now = new Date();
        return in.replace("@date", datef.format(now))
                .replace("@time", timef.format(now));
    }

    //readArgs iterates through args to record user-specified options and arguments
    //records options in operations to be executed later
    public static void readArgs(String[] args) {
        //this will track which index in the arraylist each string to replace is at
        int replacementIndex = 0;
        for (int i = 0; i < args.length; i++) {
            if (args[i].charAt(0) == '-') {
                if (args[i].equals("-help")) {
                    //the program will do nothing except print a helpful message and exit
                    giveHelp();
                } else if (args[i].equals("-prefix")) {
                    //increment i by the number of arguments as the function will handle them
                    i += parsePrefAndSuf(args, i + 1, true);
                } else if (args[i].equals("-suffix")) {
                    //increment i by the number of arguments as the function will handle them
                    i += parsePrefAndSuf(args, i + 1, false);
                } else if (args[i].equals("-replace")) {
                    parseReplace(args, i + 1);
                    //the program will remember where each string to replace is stored
                    operations.add(new Option(OptionType.REPLACE, Integer.toString(replacementIndex)));
                    replacementIndex++;
                    //increment i by 2 as the function already read them in (assuming no error)
                    i += 2;
                } else if (args[i].equals("-file")) {
                    //increment i by the number of arguments as the function will handle them
                    i += parseFile(args, i + 1);
                } else {
                    System.out.print("ERROR. Invalid option: " + args[i] + ". See the following manual.\n");
                    giveHelp();
                }
            } else {
                System.out.print("ERROR. An option must be marked with (-). See the following manual.\n");
                giveHelp();
            }
        }
        //check that we have at least one option and one filename
        if (operations.size() == 0) {
            System.out.print("ERROR. At least one option must be specified. See the following manual.\n");
            giveHelp();
        } else if (files.size() == 0) {
            System.out.print("ERROR. At least one file name must be given. See the following manual.\n");
            giveHelp();
        }
    }

    //giveHelp prints a helpful message and exits
    public static void giveHelp() {
        System.out.print(helpMsg);
        System.exit(0);
    }

    //parsePrefAndSuf reads in args starting from index until reaching an invalid argument,
    // then concatenates them and stores in operations (with key depending on isPrefix)
    //return value is the number of args, so the caller function can skip checking them
    public static int parsePrefAndSuf(String[] args, int index, boolean isPrefix) {
        String theStr = "";
        int argsProcessed = 0;
        for (int i = index; i < args.length; i++) {
            //iterate until an option is reached
            if (args[i].charAt(0) != '-') {
                theStr += formatDT(args[i]);
                argsProcessed++;
            } else {break;}
        }
        if (theStr.equals("")) {
            System.out.print("ERROR. Cannot have -prefix or -suffix without arguments. See the following manual.\n");
            giveHelp();
        }
        if (isPrefix) {
            operations.add(new Option(OptionType.PREFIX, theStr));
        } else {
            operations.add(new Option(OptionType.SUFFIX, theStr));
        }
        return argsProcessed;
    }

    //parseReplace reads in args starting from index, recording the next 2 args for -replace
    public static void parseReplace(String[] args, int index) {
        //check if there even are 2 more args, and check if they're not options
        if (args.length - index <= 2 ||
                args[index].charAt(0) == '-' || args[index + 1].charAt(0) == '-') {
            System.out.print("ERROR. -replace must take exactly 2 arguments. See the following manual.\n");
            giveHelp();
        } else if (args[index].equals(args[index + 1])) {
            System.out.print("ERROR. You cannot replace a string with itself. See the following manual.\n");
            giveHelp();
        } else {
            toBeReplaced.add(formatDT(args[index]));
            toReplaceWith.add(formatDT(args[index + 1]));
            replacedStrFound.add(false);
        }
    }

    //parseFile reads in args starting from index, recording valid file names to be modified
    //return value is the number of args, so the caller function can skip checking them
    public static int parseFile(String[] args, int index) {
        int fileCount = 0;
        String curName = "";
        for (int i = index; i < args.length; i++) {
            //iterate until an option is reached
            if (args[i].charAt(0) != '-') {
                curName = args[i];
                //add file to arraylist of files if it is valid
                File f = new File(formatDT(curName));
                try {
                    if (f.exists()) {
                        files.add(f);
                        fileCount++;
                    } else {
                        System.out.print("ERROR. File " + curName + " not found. Ending Program.\n");
                        System.exit(0);
                    }
                } catch (SecurityException e) {
                    System.out.print("ERROR. Cannot access file " + curName + ". Ending program.\n");
                    System.exit(0);
                }
            } else {break;}
        }
        //report lack of file names.
        if (fileCount == 0) {
            System.out.print("ERROR. Failed to specify file name. See the following manual.\n");
            giveHelp();
        }
        return fileCount;
    }

    //makeNames renames each file in files using the operations recorded
    public static void makeNames() {
        for (File f: files) {
            //used to store the name as operations are applied
            String newName = f.getName();
            //used to show the next operation
            String nextTransform;
            //each key will be PREFIX, SUFFIX, or REPLACE
            for (Option o: operations) {
                OptionType ot = o.getType();
                if (ot.equals(OptionType.PREFIX)) {
                    nextTransform = o.getArg().concat(newName);
                    System.out.println("Prefix: " + newName + " becomes " + nextTransform);
                    newName = nextTransform;
                } else if (ot.equals(OptionType.SUFFIX)) {
                    nextTransform = newName.concat(o.getArg());
                    System.out.println("Suffix: " + newName + " becomes " + nextTransform);
                    newName = nextTransform;
                } else { //replace
                    int theIndex = Integer.parseInt(o.getArg());
                    if (newName.contains(toBeReplaced.get(theIndex))) {
                        nextTransform = newName.replace(toBeReplaced.get(theIndex), toReplaceWith.get(theIndex));
                        replacedStrFound.set(theIndex, true);
                        System.out.println("Replace: " + newName + " becomes " + nextTransform);
                        newName = nextTransform;
                    }
                }
            }
            try {
                File dest = new File(newName);
                f.renameTo(dest);
                System.out.println("File renaming complete.");
            } catch (SecurityException e) {
                System.out.print("ERROR. Lack permission to rename to " + newName + ". Ending program.\n");
                System.exit(0);
            }
        }
        //if -replace was called but no string matching the arg was found, report to user
        for (int i = 0; i < toBeReplaced.size(); i++) {
            if (!replacedStrFound.get(i)) {
                System.out.print("WARNING. None of the file names contained " + toBeReplaced.get(i) + ". Replacement failed.\n");
            }
        }
        System.out.println("All files successfully renamed.");
    }

    public static void main(String[] args) {
        readArgs(args);
        makeNames();
    }
}